## Project Summary
In this project, by employing Spark, we perform extract-load-transform of the I94 immigration, global land temperatures and US demographics data to create a star schema database on immigration.

The data for this project can be loaded in the udacity workspace, or from the pre-loaded S3 bucket. The codes consist of the following files:

* etl.py: read I94 data, and call functions to create dimension tables and fact tables. There is a flag write_data_to_S3 to choose whether or not to write data to the S3 bucket. This flag is designated for debugging purpose.
* load_transform_tables.py: Functions to create dimension tables and fact tables.
* dl.cfg: Credentials to access the S3 bucket.
* CapstoneProject.ipynb: A Jupyter notebook for verifying data and construct the ETL pipeline.


Step 5: Complete Project Write Up

### Step 1: Scope the Project and Gather Data
Load the I94 data, global land temperature data, and demographics data, identify columns to be dropped from the above datasets, finally, perform data cleaning functions on all the datasets. Some columns need to be renamed for good matching between the dimension tables and fact table.\
Create visatype dimension table from I94 data, which is connected to the fact table by 'visa_id'.\
Create calendar dimension table from I94 data, which is connected to the fact table by 'arrdate'.\
Process temperature data by dropping all n/a in AverageTemperature, then calculate the average temperature (avg_temp) for each country.\
Create country dimension table from I94 data with avg_temp. This table is connected to the fact table by 'country_code'.\
Create demographics dimension table from the demographics data. This table is connected to the fact table by 'state_code'.\
Finally, create fact table from the I94 immigration data and the visatype data.

### Step 2: Explore and Assess the Data
Exploration of data is shown in CapstoneProject.ipynb.

### Step 3: Define the Data Model
3.1 Conceptual Data Model
![Starschema for the Data Model](diagram.png)

3.2 Mapping Out Data Pipelines
* Load the datasets either from workspace or AWS S3.
* Clean the I94 data.
* Create visatype dimension table
* Create calendar dimension table
* Process global temperatures data
* Create country dimension table
* Create demographics dimension table
* Create immigration fact table

### Step 4: Run ETL to Model the Data
Create the data model and perform data quality check.

### To run the ETL pipeline: Execute etl.py.