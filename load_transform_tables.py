import pandas as pd
import configparser
import os
import datetime as dt
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.functions import to_timestamp
from pyspark.sql.functions import col, hour, dayofmonth, dayofweek, month, year, weekofyear
from pyspark.sql.functions import monotonically_increasing_id
from pyspark.sql.functions import upper, col
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"
os.environ["PATH"] = "/opt/conda/bin:/opt/spark-2.4.3-bin-hadoop2.7/bin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-8-openjdk-amd64/bin"
os.environ["SPARK_HOME"] = "/opt/spark-2.4.3-bin-hadoop2.7"
os.environ["HADOOP_HOME"] = "/opt/spark-2.4.3-bin-hadoop2.7"

get_datetime = udf(lambda x: (dt.datetime(1960, 1, 1).date() + dt.timedelta(x)).isoformat() if x else None)

def quality_check(df):
    """Count number of data points in a dataframe.
    :param table: corresponding name of table
    """
    total = df.count()
    
    df.printSchema()
    
    if total == 0:
        print(f"Data quality check failed\n")
    else:
        print(f"Data quality check passed\n")
    return 0

def create_visa_dimension_table(i94_data, write_data_to_S3, s3_path):
    """
    Create the visatype dimension table from the I94 data.
    :param i94_data: Input I94 dataframe
    :param write_data_to_S3: Flag to write output to S3 or not (for debugging purpose)
    :param s3_path: Path to write output data on S3
    :return: dataframe for the visatype dimension table
    """
    # create visatype dataframe from visatype column with primary key "visa_id"
    visatype_data = i94_data.select(['visatype']).distinct() \
        .withColumn('visa_id', monotonically_increasing_id())
    
    print(f'Finish processing visatype data')
    
    # check data quality
    quality_check(visatype_data)
    
    # write output to parquet file
    if write_data_to_S3:
        print(f'Write visatype data to S3')
        visatype_data.write.parquet(os.path.join(s3_path, 'visa_type'), 'overwrite')
    
    return visatype_data

def create_calendar_dimension_table(i94_data, write_data_to_S3, s3_path):
    """
    Create the calendar dimension table from the I94 data.
    :param i94_data: Input I94 dataframe
    :param write_data_to_S3: Flag to write output to S3 or not (for debugging purpose)
    :param s3_path: Path to write output data on S3
    :return: dataframe for the calendar dimension table
    """

    # create calendar dataframe by selecting arrdate from the I94 data
    calendar_data = i94_data.select(['arrdate']) \
        .withColumn('arrdate', get_datetime(i94_data.arrdate)).distinct()

    # extract "arrdate" data to day, week, month, year, weekday
    calendar_data = calendar_data.withColumn('arrday', dayofmonth('arrdate')) \
        .withColumn('arrweek', weekofyear('arrdate')) \
        .withColumn('arrmonth', month('arrdate')) \
        .withColumn('arryear', year('arrdate')) \
        .withColumn('arrweekday', dayofweek('arrdate')) \
        .withColumn('calendar_id', monotonically_increasing_id())
    
    print(f'Finish processing calendar data')
    
    # check data quality
    quality_check(calendar_data)
    
    # write output to parquet file, partitioned by "arryear" and "arrmonth"
    if write_data_to_S3:
        print(f'Write calendar data to S3')
        calendar_data.write.partitionBy('arryear', 'arrmonth') \
            .parquet(os.path.join(s3_path, 'calendar'), 'overwrite')

    return calendar_data

def create_temperature_table(spark, s3_path):
    """
    Create a temporary temperature table from the GlobalLandTemperaturesByCity.csv data.
    :param spark: Spark session
    :param s3_path: Path to read input data from S3
    :return: dataframe for the temperature table
    """
    
    # read temperature data from GlobalLandTemperaturesByCity.csv in the S3 bucket
    #temperature_data = spark.read.csv(s3_path + 'GlobalLandTemperaturesByCity.csv', \
    #    header = True, inferSchema = True)
    
    # read temperature data from GlobalLandTemperaturesByCity.csv in udacity workspace
    temperature_data = spark.read.csv('../../data2/GlobalLandTemperaturesByCity.csv', \
        header = True, inferSchema = True)
    
    # drop invalid data with no AverageTemperature,
    # then drop duplication of ('dt', 'City', 'Country')
    # then calculate average temperature by country
    # then convert column 'Country' to uppercase
    
    temperature_data = temperature_data.dropna(subset = ['AverageTemperature']) \
    .drop_duplicates(subset = ['dt', 'City', 'Country']) \
    .select(['Country', 'AverageTemperature']) \
    .groupby('Country').avg() \
    .withColumnRenamed('avg(AverageTemperature)', 'avg_temp') \
    .select('*', upper(col('Country'))) \
    .drop('Country') \
    .withColumnRenamed('upper(Country)', 'country')
    
    print(f'Finish processing temperature data')
    
    return temperature_data

def create_country_dimension_table(spark, i94_data_org, temperature_data_org, write_data_to_S3, s3_path):
    """
    Create the country dimension table from the I94 data and temperature data.
    :param spark: Spark session
    :param i94_data_org: Original input I94 dataframe
    :param temperature_data_org: Original input temperature data
    :param write_data_to_S3: Flag to write output to S3 or not (for debugging purpose)
    :param s3_path: Path to write output data on S3.
    :return: dataframe for the country dimension table
    """
    
    # create a local temporary view for i94_data
    i94_data_org.createOrReplaceTempView("i94_data")
    
    # load country codes from country_codes.csv in workspace
    country_codes_org = spark.read.csv('country_codes.csv', header = True, inferSchema = True)
    
    # load country codes from country_codes.csv in the S3 bucket
    #country_codes_org = spark.read.csv(s3_path + 'country_codes.csv', header = True, inferSchema = True)
    
    # create a local temporary view for country_codes
    country_codes_org.createOrReplaceTempView("country_codes")
    
    # create a local temporary view for temperature_data
    temperature_data_org.createOrReplaceTempView("temperature_data")
    
    # join i94_data and country_codes
    country_data = spark.sql("""
        SELECT i94res as country_code, country
        FROM i94_data
        LEFT JOIN country_codes
        ON i94_data.i94res = country_codes.code
        """).distinct()
    
    # create a local temporary view for country_data
    country_data.createOrReplaceTempView("country_tmp")
    
    # join country_tmp and temperature_data
    country_data = spark.sql("""
        SELECT country_tmp.country_code, country_tmp.country, temperature_data.avg_temp
        FROM country_tmp
        LEFT JOIN temperature_data
        ON country_tmp.country = temperature_data.country
        """).distinct()
    
    print(f'Finish processing country data')
    
    # check data quality
    quality_check(country_data)
    
    # write output to parquet file
    if write_data_to_S3:
        print(f'Write country data to S3')
        country_data.write.parquet(os.path.join(s3_path + "country"), mode = "overwrite")
    
    return country_data

def process_demographics_data(spark, write_data_to_S3, s3_path):
    """
    Create the demographics dimension table from the us-cities-demographics.csv data.
    :param spark: Spark session
    :param write_data_to_S3: Flag to write output to S3 or not (for debugging purpose)
    :param s3_path: Path to write output data on S3
    :return: dataframe for the demographics dimension table
    """
    
    # load original demographics data from the S3 bucket
    #org_data = spark.read.csv(s3_path + 'us-cities-demographics.csv', inferSchema = True, header = True, sep = ';')
    
    # load original demographics data from udacity workspace
    org_data = spark.read.csv('us-cities-demographics.csv', inferSchema = True, header = True, sep = ';')
    
    # drop invalid rows, and rename the rows so that they can be written to S3
    demographics_data = org_data.withColumnRenamed('Median Age', 'median_age') \
    .withColumnRenamed('Male Population', 'male_pop') \
    .withColumnRenamed('Female Population', 'female_pop') \
    .withColumnRenamed('Total Population', 'pop') \
    .withColumnRenamed('Number of Veterans', 'no_vetr') \
    .withColumnRenamed('Foreign-born', 'foreign_born') \
    .withColumnRenamed('Average Household Size', 'avg_household') \
    .withColumnRenamed('State Code', 'state_code') \
    .dropna(subset = ['male_pop', 'female_pop', 'no_vetr', 'foreign_born', 'avg_household']) \
    .dropDuplicates(subset = ['City', 'State', 'state_code', 'Race'])
    
    # insert primary key 'demograph_id'
    demographics_data = demographics_data.withColumn('demograph_id', monotonically_increasing_id())
    
    print(f'Finish processing demographics data')
    
    # check data quality
    quality_check(demographics_data)
    
    # write output to parquet file
    if write_data_to_S3:
        print(f'Write demographics data to S3')
        demographics_data.write.parquet(os.path.join(s3_path, "demographics"), mode = "overwrite")
    
    return demographics_data

def create_immigration_fact_table(spark, immigration_data_org, visa_data_org, write_data_to_S3, s3_path):
    """
    Create the immigration fact table from the I94 data.
    :param spark: Spark session
    :param immigration_data_org: Original I94 dataframe
    :param visa_data_org: Visa data
    :param write_data_to_S3: Flag to write output to S3 or not (for debugging purpose)
    :param s3_path: Path to write output data on S3
    :return: dataframe for the immigration fact table
    """
    
    # create a local temporary view for visa_data
    visa_data_org.createOrReplaceTempView("visa_data")

    # create a local temporary view for immigration_data
    immigration_data_org.createOrReplaceTempView("immigration_data")

    # join immgration_data and visa_data
    # rename 'i94res' to 'country', and 'i94addr' to 'state_code'
    immigration_data = spark.sql(
        """SELECT immigration_data.*, visa_data.visa_id
        FROM immigration_data
        LEFT JOIN visa_data ON visa_data.visatype = immigration_data.visatype
        """).withColumnRenamed('i94res', 'country_code') \
       .withColumnRenamed('i94addr', 'state_code')

    # extract "arrdate" data to day, week, month, year, weekday, then drop visatype
    immigration_data = immigration_data \
        .withColumn('arrdate', get_datetime(immigration_data.arrdate)) \
        .drop('visatype')
    
    print(f'Finish processing immigration data')
    
    # check data quality
    quality_check(immigration_data)
    
    # write output to parquet file
    if write_data_to_S3:
        print(f'Write country data to S3')
        immigration_data.write.parquet(os.path.join(s3_path, "immigration"), mode = "overwrite")

    return immigration_data