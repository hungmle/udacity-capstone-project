import pandas as pd
import configparser
import os
from pyspark.sql import SparkSession
import load_transform_tables
config = configparser.ConfigParser()
config.read('dl.cfg')
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"
os.environ["PATH"] = "/opt/conda/bin:/opt/spark-2.4.3-bin-hadoop2.7/bin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-8-openjdk-amd64/bin"
os.environ["SPARK_HOME"] = "/opt/spark-2.4.3-bin-hadoop2.7"
os.environ["HADOOP_HOME"] = "/opt/spark-2.4.3-bin-hadoop2.7"
os.environ['AWS_ACCESS_KEY_ID'] = config['KEYS']['AWS_ACCESS_KEY_ID']
os.environ['AWS_SECRET_ACCESS_KEY'] = config['KEYS']['AWS_SECRET_ACCESS_KEY']

def create_spark_session():
    """
    Create Spark session
    """
    spark = SparkSession \
        .builder \
        .config("spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.7.0") \
        .config("spark.debug.maxToStringFields", "50") \
        .getOrCreate()
    
    return spark

def main():
    """
    Execute ETL
    """
    
    #This is a flag indicating whether or not to write transformed data to S3
    global write_data_to_S3
    write_data_to_S3 = True
    
    # create Spark session
    spark = create_spark_session()
    
    # S3 path to output data
    s3_path = "s3a://hung-s3/capstone/"
    
    # load I94 data from files and drop data with all n/a entries
    #i94_data = spark.read.load('./sas_data').dropna(how = 'all')
    fname = '../../data/18-83510-I94-Data-2016/i94_apr16_sub.sas7bdat'
    i94_data = pd.read_sas(fname, 'sas7bdat', encoding="ISO-8859-1").dropna(how = 'all')
    
    # create dimension table: visa_data
    visa_data = load_transform_tables.create_visa_dimension_table(i94_data, write_data_to_S3, s3_path)
    
    # create dimension table: calendar_data
    calendar_data = load_transform_tables.create_calendar_dimension_table(i94_data, write_data_to_S3, s3_path)
    
    # process temperature data
    temperature_data = load_transform_tables.create_temperature_table(spark, s3_path)
    
    # create dimension table: country_data
    country_data = load_transform_tables.create_country_dimension_table(spark, i94_data, temperature_data, write_data_to_S3, s3_path)
    
    # create dimension table: demographics_data
    demographics_data = load_transform_tables.process_demographics_data(spark, write_data_to_S3, s3_path)
    
    # create fact table: immigration_data
    immigration_data = load_transform_tables.create_immigration_fact_table(spark, i94_data, visa_data, write_data_to_S3, s3_path)
    
    return 0

if __name__ == "__main__":
    main()